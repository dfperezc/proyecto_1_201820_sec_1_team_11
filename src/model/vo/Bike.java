package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>
{

	private int bikeId;
	private int totalTrips;
	private double totalDistance;
	private int totalDuration;

	public Bike(int bikeId, int totalTrips, double totalDistance, int ptotalDuration)
	{
		this.bikeId = bikeId;
		this.totalTrips = totalTrips;
		this.totalDistance = totalDistance;
		this.totalDuration = ptotalDuration;
	}

	@Override
	public int compareTo(Bike bi)
	{
		if (this.totalDistance == bi.getTotalDistance())
			return 0;
		else if (this.totalDistance < bi.getTotalDistance())
			return -1;
		else
			return 1;
	}

	public int compareToDuration(Bike bi)
	{
		if (this.totalDuration == bi.getTotalDuration())
			return 0;
		else if (this.totalDuration < bi.getTotalDuration())
			return -1;
		else
			return 1;
	}

	public int getBikeId()
	{
		return bikeId;
	}

	public int getTotalTrips()
	{
		return totalTrips;
	}
	
	public void setTotalTrips(int totalTrips)
	{
		this.totalTrips = totalTrips;
	}

	public double getTotalDistance()
	{
		return totalDistance;
	}
	
	public void setTotalDistance(double totalDistance)
	{
		this.totalDistance=totalDistance;
	}
	
	public int getTotalDuration()
	{
		return totalDuration;
	}
	
	public void setTotalDuration(int totalDuration)
	{
		this.totalDuration=totalDuration;
	}
}